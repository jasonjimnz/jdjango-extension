# README #

* A Django web-framework extension for base backend/CMS development in Python
* Alpha 0.0.1
https://bitbucket.org/jasonjimnz/jdjango-extension

### Installation ###

STRONGLY RECOMMENDED PYTHON VIRTUALENV

- Python 2.7.2 (I have not tried in Python3 but it should run in Python 3)
- PIP (For dependencies)
- Django >= 1.8 (I'm developing in Mac Os X under Django 1.8 and Ubuntu 15.04 under Django 1.9.2, it runs OK).

It's not a Python/Django CMS, this is an extension to make easier, blog, e-commerce, e-learning, forum sites, etc...

JDjango Extension Copyright (C) 2016  Jason Jiménez Cruz http://www.jasonjimnzdev.es jasonjimenezcruz@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.