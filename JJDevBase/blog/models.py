# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MaxValueValidator, MinValueValidator
from core.models import ModeloBase, ManagerBorradoLogico, Configuracion, Categorizable, Etiquetable
from core.models import Comentable, Valorable, Seguible, Likeable, Notificable, Reportable

class BlogPost(ModeloBase, Categorizable, Etiquetable, Comentable, Valorable, Notificable, Likeable, Reportable):
    id = models.AutoField(primary_key=True)
    author = models.ForeignKey(
        'core.Perfil',
        verbose_name=_(u"Autor"),
        help_text=_(u"Usuario que escribe el post"),
        related_name="blog_posts_author"
        )
    title = models.CharField(
        max_length=160,
        verbose_name=_(u"Título"),
        help_text=_(u"Título de la entrada"),
        )
    slug = models.CharField(
        max_length=160,
        verbose_name=_(u"Slug"),
        help_text=_(u"Nombre clave para la URL"),
        )
    content = models.TextField(
        verbose_name=_(u"Contenido"),
        help_text=_(u"Contenido de la entrada"),
        )
    seo_description = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción (SEO)"),
        help_text=_(u"Descripción de la entrada para motores de búsqueda"),
        blank=True,
        null=True,
        )
    seo_keywords = models.CharField(
        max_length=500,
        verbose_name=_(u"Palabras clave (SEO)"),
        help_text=_(u"Palabras clave para motores de búsqueda"),
        blank=True,
        null=True,
        )
    draft = models.BooleanField(
        default=False,
        verbose_name=_(u"Borrador"),
        help_text=_(u"Estado borrador para la entrada")
        )
    public = models.BooleanField(
        default=True,
        verbose_name=_(u"Público"),
        help_text=_(u"Estado de visibilidad para no usuarios"),
        )

    def __unicode__(self):
        return self.title

    # It Returns the short content of a post, if have more than 500 chars.
    @classmethod
    def _get_short_content(self):
        return self.content[:500]

    class Meta:
        verbose_name = _(u"Entrada Blog")
        verbose_name_plural = _(u"Entradas Blog")
        unique_together = ('slug', 'draft', 'alive')

class UserBlog(ModeloBase, Categorizable, Etiquetable, Comentable, Notificable, Seguible):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(
        'core.Perfil',
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil al cual pertenece el blog"),
        related_name="user_blogs_owner",
        )
    title = models.CharField(
        max_length=160,
        verbose_name=_(u"Título"),
        help_text=_(u"Título para el blog"),
        )
    slug = models.CharField(
        max_length=160,
        verbose_name=_(u"Slug"),
        help_text=_(u"URL del blog"),
        )
    description = models.CharField(
        max_length=1000,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del blog del usuario"),
        blank=True,
        null=True
        )
    active = models.BooleanField(
        default=False,
        verbose_name=_(u"Activo"),
        help_text=_(u"Estado de activación del blog del usuario"),
        )
    private = models.BooleanField(
        default=False,
        verbose_name=_(u"Privado"),
        help_text=_(u"Si activas esta casilla sólo la gente que sigas podrá leer tu blog"),
        )

    def __unicode__(self):
        return self.owner.get_profile_name() + " : " + self.title


    class Meta:
        verbose_name = _(u"Blog de usuario")
        verbose_name_plural = _(u"Blogs de usuarios")
        unique_together = ('slug', 'owner', 'alive')

class UserBlogPost(ModeloBase, Categorizable, Etiquetable, Comentable, Notificable, Seguible, Reportable):
    id = models.AutoField(primary_key=True)
    blog = models.ForeignKey(
        UserBlog,
        verbose_name=_(u"Blog"),
        help_text=_(u"Blog al que pertenece el artículo"),
        related_name="user_blog_posts_blog",
        )
    title = models.CharField(
        max_length=160,
        verbose_name=_(u"Título"),
        help_text=_(u"Título del artículo"),
        )
    slug = models.CharField(
        max_length=160,
        verbose_name=_(u"Slug"),
        help_text=_(u"URL para el el artículo"),
        )
    content = models.TextField(
        verbose_name=_(u"Contenido"),
        help_text=_(u"Contenido del artículo"),
        )
    draft = models.BooleanField(
        default=False,
        verbose_name=_(u"Borrador"),
        help_text=_(u"Estado de borrador para el artículo"),
        )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u"Blog de usuario")
        verbose_name_plural = _(u"Blogs de usuarios")
        unique_together = (('blog','slug', 'alive'), ('blog','title','alive'))

