from django.contrib import admin
from core.models import Usuario, Configuracion, Perfil, Comentable
from core.models import Adjuntable, Valorable, Puntuable, Observable
from core.models import Seguible, Mensajeable, Notificable, Compartible
from core.models import Bloqueable, TipoFichero, Adjunto, TipoCategoria
from core.models import Likeable, Reportable, Categorizable, Comentario
from core.models import Valoracion, Puntuacion, Mensaje, TipoNotificacion
from core.models import Categoria, ComentableComentario, AdjuntableAdjunto
from core.models import ValorableValoracion, LikeableLike
from core.models import NotificableNotificacion, ObservableObservado
from core.models import PuntuablePuntuacion, SeguibleSeguido
from core.models import MensajeableMensaje, Notificacion
from core.models import CompartibleCompartido, BloqueableBloqueado
from core.models import ReportableReportado, CategorizableCategoria
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Configuracion)
admin.site.register(Perfil)
admin.site.register(Comentable)
admin.site.register(Adjuntable)
admin.site.register(Valorable)
admin.site.register(Puntuable)
admin.site.register(Observable)
admin.site.register(Seguible)
admin.site.register(Mensajeable)
admin.site.register(Notificable)
admin.site.register(Compartible)
admin.site.register(Bloqueable)
admin.site.register(TipoFichero)
admin.site.register(Adjunto)
admin.site.register(TipoCategoria)
admin.site.register(Likeable)
admin.site.register(Reportable)
admin.site.register(Categorizable)
admin.site.register(Comentario)
admin.site.register(Valoracion)
admin.site.register(Puntuacion)
admin.site.register(Mensaje)
admin.site.register(TipoNotificacion)
admin.site.register(Categoria)
admin.site.register(ComentableComentario)
admin.site.register(AdjuntableAdjunto)
admin.site.register(ValorableValoracion)
admin.site.register(LikeableLike)
admin.site.register(NotificableNotificacion)
admin.site.register(PuntuablePuntuacion)
admin.site.register(SeguibleSeguido)
admin.site.register(MensajeableMensaje)
admin.site.register(CompartibleCompartido)
admin.site.register(BloqueableBloqueado)
admin.site.register(ObservableObservado)
admin.site.register(ReportableReportado)
admin.site.register(CategorizableCategoria)
admin.site.register(Notificacion)
