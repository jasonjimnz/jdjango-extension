# -*- coding: utf-8 -*-
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView, View
from django.views.generic import ListView, TemplateView, DetailView
from django.http import HttpRequest, HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.template import RequestContext
from django.contrib.auth import login
from django.contrib import messages
from django.shortcuts import render

import hashlib, random
import datetime
import traceback
import os
import string


class BasicUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(BasicUserView, self).dispatch(request)


class NonRegisteredUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(NonRegisteredUserView, self).dispatch(request)


class EditorUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(EditorUserView, self).dispatch(request)


class ModeratorUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(ModeratorUserView, self).dispatch(request)


class AdminUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(AdminUserView, self).dispatch(request)


class SuperadminUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(SuperadminUserView, self).dispatch(request)


class ApiNonUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(ApiNonUserView, self).dispatch(request)


class ApiUserView(View):

	def dispatch(self, request, *args, **kwargs):

		return super(ApiUserView, self).dispatch(request)
