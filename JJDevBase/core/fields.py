# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.db import models


class LiveField(models.NullBooleanField):
    '''Similar a BooleanField, pero almacena los False como NULL.

    '''
    description = _(u'Estado del borrado lógico')
    __metaclass__ = models.SubfieldBase

    def __init__(self, *args, **kwargs):
        kwargs['null'] = True
        kwargs['default'] = True
        super(LiveField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        if value:
            return True
        return None

    def to_python(self, value):
        return bool(value)

    def get_prep_lookup(self, lookup_type, value):
        if lookup_type == 'exact' and not value:
            msg = ("%(model)s doesn't support filters with "
                "%(field)s=False. Use a filter with "
                "%(field)s=None or an exclude with "
                "%(field)s=True instead.")
            raise TypeError(msg % {
                'model': self.model.__name__,
                'field': self.name})

        return super(LiveField, self).get_prep_lookup(lookup_type, value)

    def deconstruct(self):
        name, path, args, kwargs = super(LiveField, self).deconstruct()
        #del kwargs['null']
        del kwargs['default']
        return name, path, args, kwargs