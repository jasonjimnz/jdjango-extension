# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib import auth
from django.db.models.query import QuerySet
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MaxValueValidator, MinValueValidator

from fields import LiveField


# Create your models here.

class QuerySetBorradoLogico(QuerySet):
    def delete(self):
        return super(QuerySetBorradoLogico, self).update(alive=False, deleted_at=timezone.now())

    def hard_delete(self):
        return super(QuerySetBorradoLogico, self).delete()

    def alive(self):
        return self.filter(alive=True)

    def dead(self):
        return self.exclude(alive=True)


class ManagerBorradoLogico(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(ManagerBorradoLogico, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return QuerySetBorradoLogico(self.model).filter(alive=True)
        return QuerySetBorradoLogico(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


MODELO_BASE_FIELDS = ['created_at', 'updated_at', 'deleted_at','alive']


class ModeloBase(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_(u'Fecha de creación'),
        help_text=_(u'Fecha y hora de creación del registro')
        )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_(u'Fecha de actualización'),
        help_text=_(u'Fecha de la última actualización del registro')
        )
    deleted_at = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_(u'Fecha de borrado'),
        help_text=_(u'Fecha de eliminación del registro')
        )
    alive = LiveField()

    objects = ManagerBorradoLogico()
    all_objects = ManagerBorradoLogico(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        self.alive = False
        self.deleted_at = timezone.now()
        self.save()

    def hard_delete(self):
        super(ModeloBase, self).delete()


class ManagerUsuario(ManagerBorradoLogico, BaseUserManager):
    def _create_user(self, username,  email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        #email = self.normalize_email(email)
        user = self.model(username=username, email=email, is_staff=is_staff, 
            is_active=True, is_superuser=is_superuser, last_login=now, **extra_fields) 
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(username, username, password, True, True,
                                 **extra_fields)


class Usuario(ModeloBase, AbstractBaseUser):
    username = models.EmailField(
        max_length=254,
        unique=True,
        verbose_name=_(u'Usuario (email)'),
        help_text=_(u'Nombre de usuario (email), el email debe de ser único')
        )
    email = models.EmailField(
        max_length=254, 
        null=True,
        unique=True,
        verbose_name=_(u'Correo electrónico'),
        help_text=_(u'Correo electrónico válido del usuario utilizado para cualquier comunicación')
        )
    is_active = models.BooleanField(
        default=True, 
        verbose_name=_(u'Activo'),
        help_text=_(u'Si desactivas el usuario, perderá la capacidad de iniciar sesión')
        )
    is_moderator = models.BooleanField(
        default=True, 
        verbose_name=_(u'Moderador'),
        help_text=_(u'Estado de Moderador del usuario')
        )
    is_editor = models.BooleanField(
        default=True, 
        verbose_name=_(u'Editor'),
        help_text=_(u'Estado de Editor del usuario')
        )         
    is_staff = models.BooleanField(
        default=False, 
        verbose_name=_(u'Administrador'),
        help_text=_(u'Si marcas el usuario como administrador, podrá acceder al panel de administración')
        )
    is_superuser = models.BooleanField(
        default=False, 
        verbose_name=_(u'SuperAdministrador'),
        help_text=_(u'Si marcas el usuario como SuperAdministrador, dispondra de control absoluto de la aplicación')
        )
    first_name = models.CharField(
        max_length=100, 
        verbose_name=_(u'Nombre'),
        help_text=_(u'Nombre real del usuario')
        )
    last_name = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_(u'Apellidos'),
        help_text=_(u'Apellidos del usuario')
        )
    tz_offset = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=_(u'Zona horaria'),
        help_text=_(u'Zona horaria del usuario.')
        )

    USERNAME_FIELD = 'username'
    #REQUIRED_FIELDS = ['first_name']

    objects = ManagerUsuario()
    all_objects = ManagerUsuario(alive_only=False)

    class Meta:
        unique_together = ('username','alive')

    def get_full_name(self):
        nombre = self.first_name
        if self.last_name:
            nombre = nombre + ' ' + self.last_name
        nombre = nombre + ' (' + self.username + ')'
        return nombre

    def get_short_name(self):
        nombre = self.first_name
        if self.last_name:
            nombre = nombre + ' ' + self.last_name
        return nombre
    
    def __unicode__(self):
        return self.get_full_name()

    def get_group_permissions(self, obj=None):
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_all_permissions"):
                permissions.update(backend.get_all_permissions(self, obj))
        return permissions

    def has_perm(self, perm, obj=None):
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        # Otherwise we need to check the backends.
        for backend in auth.get_backends():
            if hasattr(backend, "has_perm"):
                if backend.has_perm(self, perm, obj):
                    return True
        return False

    def has_perms(self, perm_list, obj=None):
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        for backend in auth.get_backends():
            if hasattr(backend, "has_module_perms"):
                if backend.has_module_perms(self, app_label):
                    return True
        return False

class Configuracion(models.Model):
    usuario = models.ForeignKey(
        Usuario,
        verbose_name=_(u'Usuario'),
        related_name='variables'
        )
    variable = models.CharField(
        max_length=100,
        verbose_name=_(u'Variable')
        )
    valor = models.CharField(
        max_length=255,
        verbose_name=_(u'Valor')
        )

    class Meta:
        unique_together = ('usuario','variable')


class Perfil(ModeloBase):
    usuario = models.ForeignKey(
        Usuario,
        verbose_name=_(u"Usuario"),
        help_text=_(u"Usuario del perfil"),
        related_name="perfiles_usuario",
        )
    id_facebook = models.CharField(
        max_length=500,
        verbose_name=_(u"ID Facebook"),
        help_text=_(u"ID de usuario de Facebook"),
        null=True,
        blank=True,
        )
    id_twitter = models.CharField(
        max_length=500,
        verbose_name=_(u"ID Twitter"),
        help_text=_(u"ID de usuario en Twitter"),
        null=True,
        blank=True,
        )
    id_google = models.CharField(
        max_length=500,
        verbose_name=_(u"ID Google+"),
        help_text=_(u"ID de usuario en Google+"),
        null=True,
        blank=True,
        )
    token_facebook = models.CharField(
        max_length=500,
        verbose_name=_(u"Token Facebook"),
        help_text=_(u"Token de sesion de Facebook"),
        null=True,
        blank=True,
        )
    token_twitter = models.CharField(
        max_length=500,
        verbose_name=_(u"Token Twitter"),
        help_text=_(u"Token de sesion de Twitter"),
        null=True,
        blank=True,
        )
    token_google = models.CharField(
        max_length=500,
        verbose_name=_(u"Token Google+"),
        help_text=_(u"Token de sesion de Google+"),
        null=True,
        blank=True,
        )
    hash_activacion = models.CharField(
        max_length=500,
        verbose_name=_(u"Hash activacion"),
        help_text=_(u"Hash de activacion del perfil"),
        null=True,
        blank=True,
        )
    hash_password = models.CharField(
        max_length=500,
        verbose_name=_(u"Hash contraseña"),
        help_text=_(u"Hash de activacion de la contraseña"),
        null=True,
        blank=True,
        )
    limite_activacion = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_(u'Fecha de limite de activacion'),
        help_text=_(u'Fehca limite para la activacion del perfil'),
        )
    blocked = models.BooleanField(
        default=False,
        verbose_name=_(u"Bloqueado"),
        help_text=_(u"Estado de bloqueo del perfil"),
        )
    avatar = models.CharField(
        max_length=500,
        verbose_name=_(u"Avatar (URL)"),
        help_text=_(u"URL del Avatar del perfil"),
        null=True,
        blank=True,
        )
    firma = models.CharField(
        max_length=1500,
        verbose_name=_(u"Firma"),
        help_text=_(u"Firma para mostrar en el perfil"),
        null=True,
        blank=True,
        )
    public = models.BooleanField(
        default=False,
        verbose_name=_(u"Publico"),
        help_text=_(u"Estado de visibilidad para el resto de usuarios"),
        )


# Clases Abstractas


class Comentable(models.Model):
    comentable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.comentable_id)


class Adjuntable(models.Model):
    adjuntable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.adjuntable_id)


class Valorable(models.Model):
    valorable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.valorable_id)


class Puntuable(models.Model):
    puntuable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.puntuable_id)


class Seguible(models.Model):
    seguible_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.seguible_id)


class Mensajeable(models.Model):
    mensajeable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.mensajeable_id)


class Notificable(models.Model):
    notificable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.notificable_id)


class Compartible(models.Model):
    compartible_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.compartible_id)


class Bloqueable(models.Model):
    bloqueable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.bloqueable_id)


class Observable(models.Model):
    observable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.observable_id)


class Likeable(models.Model):
    likeable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.likeable_id)


class Reportable(models.Model):
    reportable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.reportable_id)


class Categorizable(models.Model):
    categorizable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.pk)

class Etiquetable(models.Model):
    etiquetable_id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return str(self.pk)

# CLASES AUXILIARES ABSTRACTAS


class Comentario(ModeloBase):
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que hace el comentario"),
        related_name='comentarios_perfil',
        )
    comentario = models.TextField(
        verbose_name=_(u"Comentario"),
        help_text=_(u"Contenido del comentario"),
        )
    visible = models.BooleanField(
        default=False,
        verbose_name=_(u"Visible"),
        help_text=_(u"Visibilidad del comentario"),
        )

    def __unicode__(self):
        return u"C.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Comentario")
        verbose_name_plural = _(u"Comentarios")


class TipoFichero(ModeloBase):
    nombre = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre del tipo de fichero"),
        )
    extension = models.CharField(
        max_length=10,
        verbose_name=_(u"Extensión"),
        help_text=_(u"Extensión del tipo de fichero"),
        )
    descripcion = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del tipo de fichero"),
        null=True,
        blank=True,
        )

    class Meta:
        verbose_name = _(u"Tipo fichero")
        verbose_name_plural = _(u"Tipos de fichero")
        unique_together = ('nombre', 'extension', 'alive')


class Adjunto(ModeloBase):
    fichero = models.FileField(
        upload_to='uploads',
        verbose_name=_(u"Fichero a subir"),
        help_text=_(u"Fichero a subir"),
        )
    tipo_fichero = models.ForeignKey(
        TipoFichero,
        verbose_name=_(u"Tipo fichero"),
        help_text=_(u"MIME-TYPE del fichero"),
        related_name='adjuntos_tipo_fichero'
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que sube el adjunto"),
        related_name='adjuntos_perfil',
        )
    descripcion = models.CharField(
        max_length=255,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del adjunto"),
        null=True,
        blank=True,
        )
    visible = models.BooleanField(
        default=True,
        verbose_name=_(u"Visible"),
        help_text=_(u"Visibilidad del comentario"),
        )
    nombre = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre identificativo para el fichero"),
        )

    class Meta:
        verbose_name = _(u"Adjunto")
        verbose_name_plural = _(u"Adjuntos")


class Valoracion(ModeloBase):
    valoracion = models.IntegerField(
        verbose_name=_(u'Valoración'),
        help_text=_(u'Valoración del usuario'),
        default=3,
        validators=[MinValueValidator(0), MaxValueValidator(5)]
        )
    comentario = models.CharField(
        max_length=500,
        verbose_name=_(u"Comentario"),
        help_text=_(u"Comentario de la valoración"),
        null=True,
        blank=True,
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que realiza la valoración"),
        related_name="valoraciones_perfil",
        )

    def __unicode__(self):
        return u"V.ID: %s"%str(self.pk)
    class Meta:
        verbose_name = _(u"Valoracion")
        verbose_name_plural = _(u"Valoraciones")


class Puntuacion(ModeloBase):
    puntuacion = models.IntegerField(
        verbose_name=_(u"Puntuación"),
        help_text=_(u"Puntuación del usuario"),
        default=5,
        validators=[MinValueValidator(0), MaxValueValidator(10)])
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que puntúa"),
        related_name="puntuaciones_perfil",
        )

    def __unicode__(self):
        return u"P.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Puntuación")
        verbose_name_plural = _(u"Puntuaciones")


class Mensaje(ModeloBase):
    asunto = models.CharField(
        max_length=255,
        verbose_name=_(u"Asunto"),
        help_text=_(u"Asunto del mensaje"),
        )
    contenido = models.CharField(
        max_length=2000,
        verbose_name=_(u"Contenido"),
        help_text=_(u"Contenido del mensaje"),
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que envía el mensaje"),
        related_name="mensajes_perfil",
        )

    def __unicode__(self):
        return u"M.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Mensaje")
        verbose_name_plural = _(u"Mensajes")


class TipoNotificacion(ModeloBase):
    nombre = models.CharField(
        max_length=160,
        verbose_name=_(u"Tipo notificación"),
        help_text=_(u"Nombre para el tipo de notificación"),
        )
    descripcion = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del tipo de notificación"),
        )

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = _(u"Tipo notificación")
        verbose_name_plural = _(u"Tipos de notificación")
        unique_together = ("nombre", "alive")


class Notificacion(ModeloBase):
    tipo = models.ForeignKey(
        TipoNotificacion,
        verbose_name=_(u"Tipo notificación"),
        help_text=_(u"Tipo de la notificación"),
        related_name="notificaciones_tipo",
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que envía la notificación"),
        related_name="notificaciones_perfil",
        )

    def __unicode__(self):
        return u"N.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Notificación")
        verbose_name_plural = _(u"Notificaciones")

class TipoCategoria(ModeloBase):
    nombre = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre del tipo de categoría"),
        )
    descripcion = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del tipo de categoría"),
        )

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = _(u"Tipo de categoría")
        verbose_name_plural = _(u"Tipos de categoría")
        unique_together = ('nombre', 'alive')


class Categoria(ModeloBase):
    nombre = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre de la categoría"),
        )
    descripcion = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción de la categoría"),
        )
    tipo = models.ForeignKey(
        TipoCategoria,
        verbose_name=_(u"Tipo"),
        help_text=_(u"Tipo de categoría"),
        related_name='categorias_nombre'
        )

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name = _(u"Categoría")
        verbose_name_plural = _(u"Categorías")
        unique_together = ('nombre', 'tipo', 'alive')

class Etiqueta(ModeloBase, Categorizable):
    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre de la etiqueta"),
        )
    description = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción de la etiqueta"),
        )
    visible = models.BooleanField(
        default=False,
        verbose_name=_(u"Visible"),
        help_text=_(u"Visibilidad de la etiqueta"),
        )

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u"Etiqueta")
        verbose_name_plural = _(u"Etiquetas")
        unique_together = ('name', 'alive')


# CLASES ENLACE


class ComentableComentario(ModeloBase, Reportable, Puntuable, Likeable, Adjuntable, Notificable, Comentable):
    id = models.AutoField(primary_key=True)
    comentable_field = models.ForeignKey(
        Comentable,
        verbose_name=_(u"Comentable"),
        help_text=_(u"Comentable asociado al comentario"),
        related_name='comentables_comentario_comentable',
        )
    comentario = models.ForeignKey(
        Comentario,
        verbose_name=_(u"Comentario"),
        help_text=_(u"Comentario asociado al comentable"),
        related_name='comentables_comentario_comentario'
        )

    def __unicode__(self):
        return u"C.C.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Comentable Comentario")
        verbose_name_plural = _(u"Comentables Comentario")
        unique_together = ('comentable_field', 'comentario', 'alive')


class AdjuntableAdjunto(ModeloBase, Reportable, Puntuable, Likeable, Comentable, Compartible):
    id = models.AutoField(primary_key=True)
    adjuntable = models.ForeignKey(
        Adjuntable,
        verbose_name=_(u"Adjuntable"),
        help_text=_(u"Adjuntable asiociado al adjunto"),
        related_name="adjuntables_adjunto_adjuntable",
        )
    adjunto = models.ForeignKey(
        Adjunto,
        verbose_name=_(u"Adjunto"),
        help_text=_(u"Adjunto asociado al adjuntable"),
        related_name="adjuntables_adjunto_adjunto",
        )

    def __unicode__(self):
        return u"A.A.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Adjuntable Adjunto")
        verbose_name_plural = _(u"Adjuntables Adjunto")
        unique_together = ('adjuntable', 'adjunto', 'alive')


class ValorableValoracion(ModeloBase, Reportable, Likeable, Notificable, Comentable):
    id = models.AutoField(primary_key=True)
    valorable = models.ForeignKey(
        Valorable,
        verbose_name=_(u"Valorable"),
        help_text=_(u"Valorable asociado a la Valoración"),
        related_name="valorables_valoracion_valorable"
        )
    valoracion = models.ForeignKey(
        Valoracion,
        verbose_name=_(u"Valoración"),
        help_text=_(u"Valoración asociada al valorable"),
        related_name="valorables_valoracion_valoracion",
        )

    def __unicode__(self):
        return "V.V.ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Valorable Valoración")
        verbose_name_plural = _(u"Valorables Valoración")
        unique_together = ('valorable', 'valoracion', 'alive')


class PuntuablePuntuacion(ModeloBase, Notificable, Comentable, Compartible):
    id = models.AutoField(primary_key=True)
    puntuable = models.ForeignKey(
        Puntuable,
        verbose_name=_(u"Puntuable"),
        help_text=_(u"Puntuable asociado a la puntaución"),
        related_name="puntuables_puntuacion_puntuable"
        )
    puntuacion = models.ForeignKey(
        Puntuacion,
        verbose_name=_(u"Puntuación"),
        help_text=_(u"Puntuación asociada al puntuable"),
        related_name="puntuables_puntuacion_puntuacion"
        )

    def __unicode__(self):
        return "P.P. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Puntuable puntuación")
        verbose_name_plural = _(u"Puntuables puntuación")
        unique_together = ('puntuable', 'puntuacion', 'alive')


class SeguibleSeguido(ModeloBase, Notificable, Likeable, Compartible, Comentable):
    id = models.AutoField(primary_key=True)
    seguible = models.ForeignKey(
        Seguible,
        verbose_name=_(u"Seguible"),
        help_text=_(u"Seguible asociado al perfil que sigue"),
        related_name="seguibles_seguido_seguible"
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Peril asociado al seguible"),
        related_name="seguibles_seguido_perfil"
        )

    def __unicode__(self):
        return "S.S. ID: %s"% str(self.pk)

    class Meta:
        verbose_name = _(u"Seguible seguido")
        verbose_name_plural = _(u"Seguibles seguido")
        unique_together = ('seguible', 'perfil', 'alive')

class MensajeableMensaje(ModeloBase, Notificable, Adjuntable, Reportable, Mensajeable):
    id = models.AutoField(primary_key=True)
    mensajeable_field = models.ForeignKey(
        Mensajeable,
        verbose_name=_(u"Mensajeable"),
        help_text=_(u"Mensajeable asociado al mensaje"),
        related_name="mensajeables_mensaje_mensajeable"
        )
    mensaje = models.ForeignKey(
        Mensaje,
        verbose_name=_(u"Mensaje"),
        help_text=_(u"Mensaje asociado al mensajeable"),
        related_name="mensajeables_mensaje_mensaje",
        )

    def __unicode__(self):
        return "M.M. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Mensajeable mensaje")
        verbose_name_plural = _(u"Mensajeables mensaje")
        unique_together = ('mensajeable_field', 'mensaje', 'alive')


class NotificableNotificacion(ModeloBase, Notificable):
    id = models.AutoField(primary_key=True)
    notificable_field = models.ForeignKey(
        Notificable,
        verbose_name=_(u"Notificable"),
        help_text=_(u"Notificable asociado a la notificación"),
        related_name="notificables_notificacion_notificable"
        )
    notificacion = models.ForeignKey(
        Notificacion,
        verbose_name=_(u"Notificación"),
        help_text=_(u"Notificación asociada al notificable"),
        related_name="notificables_notificacion_notificacion"
        )

    def __unicode__(self):
        return "N.N. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Notificable notificación")
        verbose_name_plural = _(u"Notificables notificación")
        unique_together = ('notificable_field', 'notificacion', 'alive')


class CompartibleCompartido(ModeloBase, Notificable):
    id = models.AutoField(primary_key=True)
    compartible = models.ForeignKey(
        Compartible,
        verbose_name=_(u"Compartible"),
        help_text=_(u"Compartible asociado al perfil que comparte"),
        related_name="compartibles_compartido_compartible"
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil asociado al compartible"),
        related_name="compartibles_compartido_perfil")

    def __unicode__(self):
        return "C.C. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Compartible compartido")
        verbose_name_plural = _(u"Compartible compartido")
        unique_together = ('compartible', 'perfil', 'alive')

class BloqueableBloqueado(ModeloBase, Notificable):
    id = models.AutoField(primary_key=True)
    bloqueable = models.ForeignKey(
        Bloqueable,
        verbose_name=_(u"Bloqueable"),
        help_text=_(u"Bloqueable asociado al perfil que bloquea"),
        related_name="bloqueables_bloqueado_bloqueable"
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil asociado al bloqueable"),
        related_name="bloqueables_bloqueado_perfil"
        )

    def __unicode__(self):
        return "B.B. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Bloqueable bloqueado")
        verbose_name_plural = _(u"Bloqueables bloqueado")
        unique_together = ('bloqueable', 'perfil', 'alive')


class ObservableObservado(ModeloBase):
    id = models.AutoField(primary_key=True)
    observable = models.ForeignKey(
        Observable,
        verbose_name=_(u"Observable"),
        help_text=_(u"Observable asociado al perfil"),
        related_name='observables_observado_observable'
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil"),
        related_name='observables_observado_perfil',
        )

    def __unicode__(self):
        return "O.O. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Observable observado")
        verbose_name_plural = _(u"Observables observado")
        unique_together = ('observable', 'perfil', 'alive')

class LikeableLike(ModeloBase, Notificable):
    id = models.AutoField(primary_key=True)
    likeable = models.ForeignKey(
        Likeable,
        verbose_name=_(u"Likeable"),
        help_text=_(u"Likeable asociado al perfil"),
        related_name='likeables_likeable_likeable'
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil asociado al likeable"),
        related_name='likeables_likeable_perfil'
        )

    def __unicode__(self):
        return "L.L. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Likeable like")
        verbose_name_plural = _(u"Likeables like")
        unique_together = ('likeable', 'perfil', 'alive')

class ReportableReportado(ModeloBase, Notificable, Categorizable):
    id = models.AutoField(primary_key=True)
    reportable = models.ForeignKey(
        Reportable,
        verbose_name=_(u"Reportable"),
        help_text=_(u"Reportable asociado al reporte"),
        related_name="reportables_reportado_reportable",
        )
    perfil = models.ForeignKey(
        Perfil,
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil asociado al reportable y reporte"),
        related_name="reportables_reportado_perfil"
        )
    contenido = models.CharField(
        max_length=1500,
        verbose_name=_(u"Contenido"),
        help_text=_(u"Contenido del reporte"),
        )

    def __unicode__(self):
        return "R.R. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Reportable reportado")
        verbose_name_plural = _(u"Reportables reportado")
        unique_together = ('reportable', 'perfil', 'alive')


class CategorizableCategoria(ModeloBase):
    id = models.AutoField(primary_key=True)
    categorizable = models.ForeignKey(
        Categorizable,
        verbose_name=_(u"Categorizable"),
        help_text=_(u"Categorizable asociado a la categoría"),
        related_name="categorizables_categoria_categorizable"
        )
    categoria = models.ForeignKey(
        Categoria,
        verbose_name=_(u"Categoría"),
        help_text=_(u"Categoría asociada al categorizable"),
        related_name="categorizableS_categoria_categoria",
        )
    oculto = models.BooleanField(
        default=False,
        verbose_name=_(u"Oculto"),
        help_text=_(u"Estado de visibilidad de la categorización"),
        )

    def __unicode__(self):
        return "C.C. ID: %s"%str(self.pk)

    class Meta:
        verbose_name = _(u"Categorizable categoria")
        verbose_name_plural = _(u"Categorizables categoria")
        unique_together = ('categorizable', 'categoria', 'alive')

class EtiquetableEtiqueta(ModeloBase):
    id = models.AutoField(primary_key=True)
    etiquetable = models.ForeignKey(
        Etiquetable,
        verbose_name=_(u"Etiquetable"),
        help_text=_(u"Etiquetable asociado a la etiqueta"),
        related_name="etiquetables_etiqueta_etiquetable"
        )
    etiqueta = models.ForeignKey(
        Etiqueta,
        verbose_name=_(u"Etiqueta"),
        help_text=_(u"Etiqueta asociada el etiquetable"),
        related_name="etiquetables_etiqueta_etiqueta",
        )

    def __unicode__(self):
        return "E.E. ID: %s"%str(self.id)

    class Meta:
        verbose_name = _(u"Etiquetable etiqueta")
        verbose_name_plural = _(u"Etiquetables etiqueta")
        unique_together = ('etiquetable', 'etiqueta', 'alive')
