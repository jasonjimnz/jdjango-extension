# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MaxValueValidator, MinValueValidator
from core.models import ModeloBase, ManagerBorradoLogico, Configuracion, Categorizable, Etiquetable
from core.models import Comentable, Valorable, Seguible, Likeable, Seguible, Reportable
from core.models import Mensajeable, Notificable, Adjuntable, Compartible


class Forum(ModeloBase, Categorizable, Etiquetable, Valorable, Likeable, Seguible, Reportable, Mensajeable, Notificable):
    id = models.AutoField(primary_key=True)
    profile = models.ForeignKey(
        'core.Perfil',
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil creador del foro"),
        related_name='forums_profile',
        )
    moderator = models.ManyToManyField(
        'core.Perfil',
        verbose_name=_(u"Moderadores"),
        help_text=_(u"Usuarios moderadores del foro"),
        related_name="forums_moderator",
        null=True,
        blank=True
        )
    name = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre del foro"),
        )
    slug = models.CharField(
        max_length=160,
        verbose_name=_(u"Slug"),
        help_text=_(u"URL del foro"),
        )
    description = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del foro"),
        blank=True,
        null=True,
        )
    descripcion_seo = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción (SEO)"),
        help_text=_(u"Descripción del foro (SEO)"),
        blank=True,
        null=True,
        )
    keywords_seo = models.CharField(
        max_length=500,
        verbose_name=_(u"Keywords (SEO)"),
        help_text=_(u"Palabras clave (SEO)"),
        blank=True,
        null=True
        )
    visible = models.BooleanField(
        default=True,
        verbose_name=_(u"Visible"),
        help_text=_(u"Visibilidad del foro")
        )
    private = models.BooleanField(
        default=False,
        verbose_name=_(u"Privado"),
        help_text=_(u"Foro privado, únicamente accesible por aceptación de su creador")
        )
    trackeable = models.BooleanField(
        default=True,
        verbose_name=_(u"Trazable"),
        help_text=_(u"Trazable la URL por el robots.txt para indexación"),
        )
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u"Foro")
        verbose_name_plural = _(u"Foros")
        unique_together = ('slug', 'visible', 'alive')


class Thread(ModeloBase, Etiquetable, Valorable, Likeable, Seguible, Reportable, Mensajeable, Notificable):
    id = models.AutoField(primary_key=True)
    profile = models.ForeignKey(
        'core.Perfil',
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que crea el hilo"),
        related_name='threads_profile'
        )
    name = models.CharField(
        max_length=160,
        verbose_name=_(u"Nombre"),
        help_text=_(u"Nombre del hilo"),
        )
    slug = models.CharField(
        max_length=160,
        verbose_name=_(u"Slug"),
        help_text=_(u"URL del hilo"),
        )
    description = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción"),
        help_text=_(u"Descripción del hilo"),
        blank=True,
        null=True,
        )
    descripcion_seo = models.CharField(
        max_length=500,
        verbose_name=_(u"Descripción (SEO)"),
        help_text=_(u"Descripción del hilo (SEO)"),
        blank=True,
        null=True,
        )
    keywords_seo = models.CharField(
        max_length=500,
        verbose_name=_(u"Keywords (SEO)"),
        help_text=_(u"Palabras clave (SEO)"),
        blank=True,
        null=True
        )
    visible = models.BooleanField(
        default=True,
        verbose_name=_(u"Visible"),
        help_text=_(u"Visibilidad del hilo")
        )
    trackeable = models.BooleanField(
        default=True,
        verbose_name=_(u"Trazable"),
        help_text=_(u"Trazable la URL por el robots.txt para indexación"),
        )

    def __unicode__(self):
        return self.name


    class Meta:
        verbose_name = _(u"Hilo")
        verbose_name_plural = _(u"Hilos")
        unique_together = ('slug', 'visible', 'alive')


class Reply(ModeloBase, Valorable, Likeable, Seguible, Reportable, Notificable, Adjuntable, Compartible):
    id = models.AutoField(primary_key=True)
    thread = models.ForeignKey(
        Thread,
        verbose_name=_(u"Hilo"),
        help_text=_(u"Hilo al que pertenece la respuesta"),
        related_name="replies_thread",
        )
    profile = models.ForeignKey(
        'core.Perfil',
        verbose_name=_(u"Perfil"),
        help_text=_(u"Perfil que responde"),
        related_name='replies_profile'
        )
    topic = models.CharField(
        max_length=180,
        verbose_name=_(u"Asunto"),
        help_text=_(u"Asunto/Título de la respuesta"),
        blank=True,
        null=True
        )
    content = models.TextField(
        verbose_name=_(u"Respuesta"),
        help_text=_(u"Respuesta"),
        )
    permalink = models.IntegerField(
        max_length=20,
        verbose_name=_(u"Identificador"),
        help_text=_(u"Identificadoŕ único de la respuesta"),
        )

    def __unicode__(self):
        return self.thread.name + ': ' + profile.get_username() + ' - ' + self.topic

    class Meta:
        verbose_name = _(u"Respuesta")
        verbose_name_plural = _(u"Respuestas")
        unique_together = ('thread', 'permalink', 'alive')
